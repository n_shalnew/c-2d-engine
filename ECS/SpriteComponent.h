//#include "ECS.h"
//
// Created by nick on 08.04.18.
//

#ifndef FIRST_SPRITECOMPONENT_H
#define FIRST_SPRITECOMPONENT_H

#include "Components.h"
#include "SDL_image.h"
#include "../TextureManager.h"

class SpriteComponent : public Component{
private:
    TransformComponent *transform;
    SDL_Texture *texture;
    SDL_Rect srcRect, desRect;
public:
    SpriteComponent() = default;
    SpriteComponent(const char* path){
        setTex(path);
    }

    void setTex(const char* path){
        texture = TextureManager::LoadTexture(path);
    }

    ~SpriteComponent(){
        SDL_DestroyTexture(texture);
    }

    void init() override {
        transform = &entity->getComponent<TransformComponent>();

        srcRect.x = srcRect.y = 0;
        srcRect.w = transform->width;
        srcRect.h = transform->height;
        desRect.w = transform->width * transform->scale;
        desRect.h = transform->height * transform->scale;
    }
    void update() override {
        desRect.x = (int)transform->position.x;
        desRect.y = (int)transform->position.y;
    }
    void draw() override {
        TextureManager::Draw(texture, srcRect,desRect);
    }
};

#endif //FIRST_SPRITECOMPONENT_H
