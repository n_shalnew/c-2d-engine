//
// Created by nick on 16.04.18.
//

#ifndef FIRST_COLLISION_H
#define FIRST_COLLISION_H


#include <SDL_image.h>

class Collision {
public:
    static bool AABB(const SDL_Rect& recA, const SDL_Rect& recB);
};


#endif //FIRST_COLLISION_H
