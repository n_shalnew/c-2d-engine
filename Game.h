//
// Created by nick on 05.04.18.
//

#ifndef FIRST_GAME_H
#define FIRST_GAME_H

//#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>

class Game {

public:
    Game();
    ~Game();


    void init(const char* title, int xpos, int ypos, int width, int hight, bool fullscreen);

    void handleEvents();
    void update();
    void render();
    void clean();

    bool runnig(){ return isRunning;}

    static SDL_Renderer* renderer;
    static SDL_Event event;
private:
    int cnt = 0;
    bool isRunning;
    SDL_Window *window;

};


#endif //FIRST_GAME_H
