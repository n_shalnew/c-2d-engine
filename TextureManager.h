//
// Created by nick on 05.04.18.
//

#ifndef FIRST_TEXTUREMANAGER_H
#define FIRST_TEXTUREMANAGER_H

#include "Game.h"

class TextureManager{
public:
    static SDL_Texture* LoadTexture(const char* fileName);
    static void Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);
};

#endif //FIRST_TEXTUREMANAGER_H
