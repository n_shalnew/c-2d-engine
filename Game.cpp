//
// Created by nick on 05.04.18.
//

#include "Game.h"
#include "TextureManager.h"
#include "Map.h"
#include "Vector2D.h"
#include "Collision.h"
#include "ECS/Components.h"

Map* map;

SDL_Renderer* Game::renderer = nullptr;
SDL_Event Game::event;
Manager manager;
auto& player(manager.addEntity());
auto & wall(manager.addEntity());

Game::Game() {};
Game::~Game() {};

void Game::init(const char *title, int xpos, int ypos, int width, int hight, bool fullscreen) {

    int flags = 0;

    if (fullscreen){
        flags = SDL_WINDOW_FULLSCREEN;
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) == 0){
        std::cout << "Subsystems initialed" << std::endl;

        window = SDL_CreateWindow(title, xpos, ypos,width, hight, flags);
        if (window){
            std::cout << "Window created\n";
        }

        renderer = SDL_CreateRenderer(window, -1, 0);
        if (renderer){
            SDL_SetRenderDrawColor(renderer, 15,15,15,255);
            std::cout << "Renderer created\n";
        }

        isRunning = true;
    } else{
        isRunning = false;
    }


    map = new Map();
    player.addComponent<TransformComponent>(2);
    player.addComponent<SpriteComponent>("../assets/player.png");
    player.addComponent<KeyboarController>();
    player.addComponent<ColliderComponent>("player");

    wall.addComponent<TransformComponent>(300.0f, 300.0f, 300, 20, 1);
    wall.addComponent<SpriteComponent>("../assets/dirt.png");
    wall.addComponent<ColliderComponent>("wall");

}

void Game::handleEvents() {

    SDL_PollEvent(&event);
    switch (event.type){
        case SDL_QUIT:
            isRunning = false;
            break;
        default:
            break;
    }
}

void Game::update() {
    manager.refresh();
    manager.update();

    if (Collision::AABB(player.getComponent<ColliderComponent>().collider,
                        wall.getComponent<ColliderComponent>().collider)){
        std::cout << "Wall hit"<<std::endl;
    }

}

void Game::render() {
    SDL_RenderClear(renderer);
    map->DrawMap();
    manager.draw();
    SDL_RenderPresent(renderer);
}

void Game::clean() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    std::cout <<"Game cleaned" << std::endl;
}