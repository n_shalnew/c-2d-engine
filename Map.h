//
// Created by nick on 06.04.18.
//

#ifndef FIRST_MAP_H
#define FIRST_MAP_H

#include "Game.h"

class Map {

public:
    Map();
    ~Map();

    void LoadMap(int arr[20][25]);
    void DrawMap();

private:
    SDL_Rect src, dest;

    SDL_Texture* dirt;
    SDL_Texture* grass;
    SDL_Texture* water;

    int map[20][25];
};


#endif //FIRST_MAP_H
