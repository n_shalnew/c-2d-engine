#include "Game.h"

Game *game = nullptr;

int main() {

    const int FPS = 60;
    const int frameDalay = 1000 / FPS;

    Uint32 frameStart;
    int frameTime;

    game = new Game();

    game->init("myEngine", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,800,640, false);

    while (game->runnig()){

        frameStart = SDL_GetTicks();

        game->handleEvents();
        game->update();
        game->render();

        frameTime =  SDL_GetTicks() - frameStart;

        if (frameDalay > frameTime){
            SDL_Delay(frameDalay-frameTime);
        }
    }

    game->clean();

    return 0;
}